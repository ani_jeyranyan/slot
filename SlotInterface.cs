﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameWithNumbers
{
    interface SlotInterface
    {
        void Start_Click(object sender, EventArgs e);
        void btn_Bet5_Click(object sender, EventArgs e);
        void btn_Bet10_Click(object sender, EventArgs e);
        void btn_Bet15_Click(object sender, EventArgs e);
        void TryAgain_Click(object sender, EventArgs e);
        void Balance_TextChanged(object sender, EventArgs e);
        void Chance_count_TextChanged(object sender, EventArgs e);
    }
}
