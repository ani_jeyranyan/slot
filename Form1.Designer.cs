﻿namespace GameWithNumbers
{
    partial class Slot
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_Start = new System.Windows.Forms.Button();
            this.Balance = new System.Windows.Forms.TextBox();
            this.setBet = new System.Windows.Forms.TextBox();
            this.btn_Bet10 = new System.Windows.Forms.Button();
            this.Chance_count = new System.Windows.Forms.TextBox();
            this.btn_Bet5 = new System.Windows.Forms.Button();
            this.btn_Bet15 = new System.Windows.Forms.Button();
            this.Number1 = new System.Windows.Forms.Label();
            this.Number3 = new System.Windows.Forms.Label();
            this.Number2 = new System.Windows.Forms.Label();
            this.TryAgain = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btn_Start
            // 
            this.btn_Start.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btn_Start.Font = new System.Drawing.Font("Arial Black", 25.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Start.ForeColor = System.Drawing.Color.Maroon;
            this.btn_Start.Location = new System.Drawing.Point(142, 215);
            this.btn_Start.Name = "btn_Start";
            this.btn_Start.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btn_Start.Size = new System.Drawing.Size(212, 93);
            this.btn_Start.TabIndex = 0;
            this.btn_Start.Text = "Start";
            this.btn_Start.UseVisualStyleBackColor = false;
            this.btn_Start.Click += new System.EventHandler(this.Start_Click);
            // 
            // Balance
            // 
            this.Balance.Font = new System.Drawing.Font("Microsoft Sans Serif", 22.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Balance.ForeColor = System.Drawing.Color.Maroon;
            this.Balance.Location = new System.Drawing.Point(455, 120);
            this.Balance.Multiline = true;
            this.Balance.Name = "Balance";
            this.Balance.Size = new System.Drawing.Size(287, 50);
            this.Balance.TabIndex = 4;
            this.Balance.Text = "Balance: 100";
            this.Balance.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.Balance.TextChanged += new System.EventHandler(this.Balance_TextChanged);
            // 
            // setBet
            // 
            this.setBet.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.setBet.Location = new System.Drawing.Point(465, 185);
            this.setBet.Multiline = true;
            this.setBet.Name = "setBet";
            this.setBet.Size = new System.Drawing.Size(267, 46);
            this.setBet.TabIndex = 6;
            this.setBet.Text = "Set the Bet:";
            // 
            // btn_Bet10
            // 
            this.btn_Bet10.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn_Bet10.ForeColor = System.Drawing.Color.Maroon;
            this.btn_Bet10.Location = new System.Drawing.Point(557, 250);
            this.btn_Bet10.Name = "btn_Bet10";
            this.btn_Bet10.Size = new System.Drawing.Size(85, 84);
            this.btn_Bet10.TabIndex = 8;
            this.btn_Bet10.Text = "10";
            this.btn_Bet10.UseVisualStyleBackColor = true;
            this.btn_Bet10.Click += new System.EventHandler(this.btn_Bet10_Click);
            // 
            // Chance_count
            // 
            this.Chance_count.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Chance_count.ForeColor = System.Drawing.Color.Maroon;
            this.Chance_count.Location = new System.Drawing.Point(93, 314);
            this.Chance_count.Multiline = true;
            this.Chance_count.Name = "Chance_count";
            this.Chance_count.Size = new System.Drawing.Size(306, 47);
            this.Chance_count.TabIndex = 10;
            this.Chance_count.Text = "Chances: 5";
            this.Chance_count.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.Chance_count.TextChanged += new System.EventHandler(this.Chance_count_TextChanged);
            // 
            // btn_Bet5
            // 
            this.btn_Bet5.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn_Bet5.ForeColor = System.Drawing.Color.Maroon;
            this.btn_Bet5.Location = new System.Drawing.Point(464, 250);
            this.btn_Bet5.Name = "btn_Bet5";
            this.btn_Bet5.Size = new System.Drawing.Size(87, 84);
            this.btn_Bet5.TabIndex = 11;
            this.btn_Bet5.Text = "5";
            this.btn_Bet5.UseVisualStyleBackColor = true;
            this.btn_Bet5.Click += new System.EventHandler(this.btn_Bet5_Click);
            // 
            // btn_Bet15
            // 
            this.btn_Bet15.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn_Bet15.ForeColor = System.Drawing.Color.Maroon;
            this.btn_Bet15.Location = new System.Drawing.Point(648, 250);
            this.btn_Bet15.Name = "btn_Bet15";
            this.btn_Bet15.Size = new System.Drawing.Size(84, 84);
            this.btn_Bet15.TabIndex = 12;
            this.btn_Bet15.Text = "15";
            this.btn_Bet15.UseVisualStyleBackColor = true;
            this.btn_Bet15.Click += new System.EventHandler(this.btn_Bet15_Click);
            // 
            // Number1
            // 
            this.Number1.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Number1.ForeColor = System.Drawing.Color.Maroon;
            this.Number1.Location = new System.Drawing.Point(80, 81);
            this.Number1.Name = "Number1";
            this.Number1.Size = new System.Drawing.Size(100, 112);
            this.Number1.TabIndex = 13;
            this.Number1.Text = "?";
            this.Number1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Number1.Click += new System.EventHandler(this.Number1_Click);
            // 
            // Number3
            // 
            this.Number3.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Number3.ForeColor = System.Drawing.Color.Maroon;
            this.Number3.Location = new System.Drawing.Point(313, 81);
            this.Number3.Name = "Number3";
            this.Number3.Size = new System.Drawing.Size(100, 112);
            this.Number3.TabIndex = 14;
            this.Number3.Text = "?";
            this.Number3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Number3.Click += new System.EventHandler(this.label1_Click);
            // 
            // Number2
            // 
            this.Number2.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Number2.ForeColor = System.Drawing.Color.Maroon;
            this.Number2.Location = new System.Drawing.Point(197, 81);
            this.Number2.Name = "Number2";
            this.Number2.Size = new System.Drawing.Size(100, 112);
            this.Number2.TabIndex = 15;
            this.Number2.Text = "?";
            this.Number2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Number2.Click += new System.EventHandler(this.Number2_Click);
            // 
            // TryAgain
            // 
            this.TryAgain.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.TryAgain.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TryAgain.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.TryAgain.Location = new System.Drawing.Point(558, 443);
            this.TryAgain.Name = "TryAgain";
            this.TryAgain.Size = new System.Drawing.Size(267, 65);
            this.TryAgain.TabIndex = 16;
            this.TryAgain.UseVisualStyleBackColor = false;
            this.TryAgain.Click += new System.EventHandler(this.TryAgain_Click);
            // 
            // Slot
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::GameWithNumbers.Properties.Resources.abstract_background_design_1297_78;
            this.ClientSize = new System.Drawing.Size(837, 520);
            this.Controls.Add(this.TryAgain);
            this.Controls.Add(this.Number2);
            this.Controls.Add(this.Number3);
            this.Controls.Add(this.Number1);
            this.Controls.Add(this.btn_Bet15);
            this.Controls.Add(this.btn_Bet5);
            this.Controls.Add(this.Chance_count);
            this.Controls.Add(this.btn_Bet10);
            this.Controls.Add(this.setBet);
            this.Controls.Add(this.Balance);
            this.Controls.Add(this.btn_Start);
            this.Name = "Slot";
            this.Text = "Slot";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_Start;
        private System.Windows.Forms.TextBox Balance;
        private System.Windows.Forms.TextBox setBet;
        private System.Windows.Forms.Button btn_Bet10;
        private System.Windows.Forms.TextBox Chance_count;
        private System.Windows.Forms.Button btn_Bet5;
        private System.Windows.Forms.Button btn_Bet15;
        public System.Windows.Forms.Label Number1;
        public System.Windows.Forms.Label Number3;
        public System.Windows.Forms.Label Number2;
        private System.Windows.Forms.Button TryAgain;
    }
}

