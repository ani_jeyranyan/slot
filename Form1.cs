﻿using System;
using System.Windows.Forms;

namespace GameWithNumbers
{
    public partial class Slot : Form, SlotInterface
    {

        private static int bet;
        private static int initialBalance = 100;
        private int chances = 5;

        public Slot()
        {
            InitializeComponent();
            disableStart();
            TryAgain.Click -= TryAgain_Click;
        }


        private int RandomNumber(int min, int max)
        {
            Random random = new Random();
            return random.Next(min, max);
        }
        
        private void getNumbers()
        {
            int num1 = 0, num2 = 0, num3 = 0;

            for (int i = 0; i < 100; i++)
            {
                num1 = RandomNumber(0, 10);
                Number1.Text = num1.ToString();
            }

            for (int i = 0; i < 1000; i++)
            {
                num2 = RandomNumber(0, 10);
                Number2.Text = num2.ToString();
            }

            for (int i = 0; i < 10000; i++)
            {
                num3 = RandomNumber(0, 10);
                Number3.Text = num3.ToString();
            }

            if (num1 + num2 + num3 == 25 || num1 + num2 + num3 == 10)
            {
                Number1.Text = num1.ToString();
                Number2.Text = num1.ToString();
                Number3.Text = num1.ToString();
                initialBalance += bet * 2;
                Balance.Text = "Balance: " + initialBalance.ToString();
                return;
            }

            if (num1 == num2 || num2 == num3 || num1 == num3)
            {
                initialBalance += bet / 2; ;
                Balance.Text = "Balance: " + initialBalance.ToString();
            }
        }

        private void enableBets()
        {
            btn_Bet5.Click += btn_Bet5_Click;
            btn_Bet10.Click += btn_Bet10_Click;
            btn_Bet15.Click += btn_Bet15_Click;
        }

        private void disableBets()
        {
            btn_Bet5.Click -= btn_Bet5_Click;
            btn_Bet10.Click -= btn_Bet10_Click;
            btn_Bet15.Click -= btn_Bet15_Click;
        }

        private void enableStart()
        {
            btn_Start.Click += Start_Click;
        }

        private void disableStart()
        {
            btn_Start.Click -= Start_Click;
        }
        public void Start_Click(object sender, EventArgs e)
        {
            --chances;
            
            if (chances == 0)
            {
                getNumbers();
                Chance_count.Text = "Chances are over!";
                disableBets();
                disableStart();
                TryAgain.Click += TryAgain_Click;
                TryAgain.Text = "Try Again?";
            }
            else
            {
                getNumbers();
                Chance_count.Text = "Chances: " + chances.ToString();
                enableBets();
                disableStart();
            }

        }
        public void btn_Bet5_Click(object sender, EventArgs e)
        {
            if (initialBalance < 5)
                return;
            
            bet = 5;
            initialBalance -= bet;
            Balance.Text = "Balance: " + initialBalance.ToString();
            setBet.Text = "Set the Bet: " + bet.ToString();

            disableBets();
            enableStart();
        }

        private void Number1_Click(object sender, EventArgs e)
        {

        }

        public void btn_Bet15_Click(object sender, EventArgs e)
        {
            if (initialBalance < 15)
                return;

            bet = 15;
            initialBalance -= bet;
            Balance.Text = "Balance: " + initialBalance.ToString();
            setBet.Text = "Set the Bet: " + bet.ToString();

            disableBets();
            enableStart();
        }


        public void btn_Bet10_Click(object sender, EventArgs e)
        {
            if (initialBalance < 10)
                return;

            bet = 10;
            initialBalance -= bet;
            Balance.Text = "Balance: " + initialBalance.ToString();
            setBet.Text = "Set the Bet: " + bet.ToString();
            disableBets();
            enableStart();
        }

        public void TryAgain_Click(object sender, EventArgs e)
        {
            initialBalance = 100;
            chances = 5;

            Chance_count.Text = "Chances: 5";
            setBet.Text = "Set the Bet: ?";
            Balance.Text = "Balance: 100";
            Number1.Text = "?";
            Number2.Text = "?";
            Number3.Text = "?";

            enableBets();

            TryAgain.Click -= TryAgain_Click;
            TryAgain.Text = "";
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void Number2_Click(object sender, EventArgs e)
        {

        }

        public void Chance_count_TextChanged(object sender, EventArgs e)
        {

        }

        public void Balance_TextChanged(object sender, EventArgs e)
        {
        }
    }
}
